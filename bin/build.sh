#!/bin/bash

# include functions
source "bin/functions"

clear

echo "-----------------------------------------------------"
echo "             Checkovanie zavislosti"
echo "-----------------------------------------------------"
echo ""

# command line programs
echo "node          $(echo_if $(program_is_installed node))"
echo "bower         $(echo_if $(program_is_installed bower))"

echo ""
echo "-----------------------------------------------------"
echo "   Buildovanie aplikacie eqini-web cez Angular CLI"
echo "-----------------------------------------------------"
echo ""

npm install

bower install

ng build --aot

echo ""
echo "-----------------------------------------------------"
echo "    Vytvorenie docker imagu cez docker engine"
echo "-----------------------------------------------------"
echo ""

docker build -t eqini-web --rm=true .

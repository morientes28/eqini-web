#!/usr/bin/env bash

# na editaciu messages pouzivam online nastroj poEditor
# link:    https://poeditor.com/projects/po_edit?id_language=147&id=115489
# login:   morientes35
# heslo:   tpsoft

clear
echo "-----------------------------------------------------"
echo "   Kopirovanie prekladov na editacny server "
echo "-----------------------------------------------------"

curl -X POST https://api.poeditor.com/v2/projects/upload \
     -F api_token="e513044bad80c5e883151e2e100b7e08" \
     -F id="115489" \
     -F updating="terms" \
     -F overwrite=1 \
     -F file=@"src/locale/messages.xmb"

echo "";

# export prekladov (toto mi zatial nefungovalo)
#curl -X POST https://api.poeditor.com/v2/projects/export \
#     -d api_token="e513044bad80c5e883151e2e100b7e08" \
#     -d id="115489" \
#     -d language="sk" \
#     -d type="xtb"

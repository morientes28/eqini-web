#!/bin/bash

# pustenie docker imagu eqini-web na localhost:80
# ak chces pustit image na inom porte zmen hodnotu PORT
clear

BUILD="bin/build.sh"
bash "$BUILD"

echo "-----------------------------------------------------"
echo "      Spustenie kontajnera eqini-web cez docker"
echo "-----------------------------------------------------"
echo ""

PORT=80

echo "... aplikacia spustena ... "
echo ""

docker run -it -p 0.0.0.0:${PORT}:80 eqini-web

import { EqiniWebPage } from './app.po';

describe('eqini-web App', () => {
  let page: EqiniWebPage;

  beforeEach(() => {
    page = new EqiniWebPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});

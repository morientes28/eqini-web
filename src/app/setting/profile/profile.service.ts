import {Injectable} from "@angular/core";
import { Http, Response } from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';

import {environment} from "../../../environments/environment";
import {Profile} from "./profile";
import {LoggerService} from "../../core/logger.service";
import {SharedService} from "../../shared/shared.service";

@Injectable()
export class ProfileService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http){
    super();
  }

  /**
   * ulozenie modeloveho objektu Profile
   * @param branch
   */
  save(profile:Profile): Observable<Profile> {

    this.logger.info('call service profile.save', profile);

    let email = this.getCurrentUser().email;
    let result = this.http.put(`${this.api}/account/users/${email}/`,
      profile,
      {headers: this.getHeaders()})
      .map(mapProfile);

    this.updateCurrentUser(profile.email);
    return result;
  }

  /**
   * ziskanie modeloveho objektu Profile podla emailu
   * @param email - unique identifikator
   */
  get(email: string): Observable<Profile> {

    this.logger.info('call service profile.get', email);

    let result = this.http.get(`${this.api}/account/users/${email}/`,
      {headers: this.getHeaders()})
      .map(mapProfile);

    return result;
  }

  /**
   * mazanie modeloveho objektu
   * @param id
   */
  drop(id: number): Observable<boolean> {
    this.logger.info('call service profile.drop', id);
    throw new Error("Neimplementovana metoda");
  }

}

/*
 * Pomocne mapovacie funkcie na mapovanie json do modeloveho objektu
 */

function mapProfile(response: Response): Profile {
  return toProfile(response.json());
}

function toProfile(r: any): Profile {
  return r;
}


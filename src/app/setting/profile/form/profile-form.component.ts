import {Component, OnInit, ViewChild} from '@angular/core';
import {Profile} from "../profile";
import {NgForm} from "@angular/forms";
import {ProfileService} from "../profile.service";
import {FlashMessagesService} from "angular2-flash-messages";

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css'],
  providers: [ ProfileService ]
})
export class ProfileFormComponent implements OnInit {

  profileForm: NgForm;
  @ViewChild('profileForm') currentForm: NgForm;

  ngAfterViewChecked() {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.profileForm) { return; }
    this.profileForm = this.currentForm;
    if (this.profileForm) {
      this.profileForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    if (!this.profileForm) { return; }
    const form = this.profileForm.form;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }

  }

  validationMessages = {
    'full_name': {
      'required':      'Meno je vyžadovaný.',
      'maxlength':     'Meno musí mať menej než 32 znakov.',
    },
    'password':{
      'required':      'Heslo je vyžadované.',
      'minlength':     'Heslo musí mať viac ako 6 znakov.',
    },
    'confirm-password':{
      'required':      'Potvrdenie hesla je vyžadované.',
      'data-validation': 'Nesprávna validácia hesla',
    },
    'email':{
      'required':      'Email je vyžadovaný.',
      'email':         'Nesprávny formát emailu',
    }
  };

  formErrors = {
    'full_name': '',
    'password': '',
    'confirm_password': '',
    'email': '',
  };

   submitted = false;
   profile: Profile;

  constructor(
    private service:ProfileService,
    private flashMessagesService: FlashMessagesService
  )
  {  }

  onSubmit() {
    this.submitted = true;
    this.profile = this.profileForm.value;
    if (this.profileForm.valid){
      this.saveProfile();
    }
  }

  saveProfile(){
    this.service.save(this.profile).subscribe();
    this.flashMessagesService.show('Profil bol uložený!', { cssClass: 'alert-success', timeout: 3000 });
  }

  ngOnInit() {
    // default profile
    this.profile = new Profile(1, '', '', '','', null, null);

    // load profile to form
    let email = this.service.getCurrentUser().email;
    this.service.get(email).subscribe(p => this.profile = p);

  }

}

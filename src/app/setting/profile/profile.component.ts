import { Component, OnInit } from '@angular/core';
import {UnitTypeEnum, UnitType} from "../../shared/shared.enums";
import {LoggerService} from "../../core/logger.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  unit : UnitType = new UnitType(UnitTypeEnum.DESHBOARD);
  constructor(
    private loggerService: LoggerService
  ) { }

  ngOnInit() {
  }

}

export class Company {

	constructor(
	    public id: number,
	    public name: string,
  ) {  }
}

export class Avatar {

	constructor(
	    public id: number,
	    public url: string,
  ) {  }
}

/* Pouzivatelov profile, ktory mu plati pri v celej aplikacii */
export class Profile {

	constructor(
	    public id: number,
	    public full_name: string,
	    public email: string,
      public password: string,
      public confirm_password: string,
	    public company: Company,
	    public avatar: Avatar
	) {  }

}

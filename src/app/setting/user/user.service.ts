import {environment} from "../../../environments/environment";
import {Injectable} from "@angular/core";
import {SharedService} from "../../shared/shared.service";
import {LoggerService} from "../../core/logger.service";
import {Http, Response} from "@angular/http";
import {User} from "./user.model";
import {Observable} from "rxjs";
import {Source, Data, Row, Action} from "../../core/datatable/datatable.model";

@Injectable()
export class UserService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http) {
    super();
  }

  /**
   * Ziskanie vsetkych userov
   * @returns {Observable<User>}
   */
  getUsers(): Observable<User[]>{

    this.logger.log('call branch service getSource');

    let result = this.http.get(`${this.api}/account/users/`,
      {headers: this.getHeaders()})
      .map(mapUsers);

    return result;

  }
  /**
   * Ziskanie source pre komponentu datatables na vykreslenie dat
   */
  getSource(users:User[]): Source{

    let rows = [];
    let d:Data;
    for(let i=0; i<users.length; i++){
      rows.push(new Row(users[i].email,
        [users[i].full_name,
          users[i].email,
          users[i].is_admin.toString(),
          users[i].is_active.toString(),
          ""]));
    }

    d = new Data(
      ["Cele meno", "Email", "Admin", "Aktivny", "Akcie"],
      rows,
      new Action(true, true, false));
    return new Source("datatable", d);
  }

  get(email:string):Observable<User>{
    this.logger.info('call user service get', email);
    let result = this.http.get(`${this.api}/account/users/${email}/`,
      {headers: this.getHeaders()})
      .map(mapUser);

    return result;
  }

  save(user: User):Observable<User>{
    this.logger.info('call user service save', user);
    let result = this.http.put(`${this.api}/account/users/${user.email}/`,
      user,
      {headers: this.getHeaders()})
      .map(mapUser);

    return result;
  }

  create(user: User):Observable<User>{
    this.logger.info('call user service create', user);
    let result = this.http.post(`${this.api}/account/users/`,
      user,
      {headers: this.getHeaders()})
      .map(mapUser);

    return result;

  }

  drop(email: string):Observable<boolean>{
    let result = this.http.delete(`${this.api}/account/users/${email}/`,
      {headers: this.getHeaders()})
      .map(mapBoolean);

    return result;
  }
}

function mapUsers(response: Response): User[] {
  return response.json().map(toUser);
}

function mapUser(response: Response): User {
  return toUser(response.json());
}

function toUser(r: any): User {
  return r;
}

function mapBoolean(response: Response): boolean {
  return response.json();
}

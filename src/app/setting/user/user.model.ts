import {Base} from "../../shared/base.model";

export class User  extends Base{

  public url: string = null;
  public email: string = null;
  public full_name: string = null;
  public is_active: boolean = false;
  public last_login: Date;
  public is_admin: boolean = false;
  public password: string = null;
  public confirmpassword: string = null;

  constructor(){
    super();
  }
}

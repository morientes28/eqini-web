import {Component, OnInit, OnChanges, Input} from '@angular/core';
import {ValidatedForm} from "../../../core/form/validated-form";
import {LoggerService} from "../../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {User} from "../user.model";
import {UserService} from "../user.service";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent extends ValidatedForm implements OnInit, OnChanges{

  @Input() user: User;
  public bTitle = "Vytvoriť používatela";

  constructor(private userService: UserService,
              private loggerService: LoggerService,
              private flashMessagesService: FlashMessagesService) {
    super();
  }

  ngOnInit() {
    this.user = new User();

    // nastavenie validacnych hlasok
    this.validationMessages = {
      'full_name': {
        'required':      'Celé meno je vyžadované.',
        'maxlength':     'Celé meno musí mať menej než 128 znakov.',
      },
      'email':{
        'maxlength':     'Email musí mať menej než 124 znakov.',
        'email':         'Nespravny format emailu',
      },
      'password':{
        'minlength':     'Heslo musí mať aspon 6 znakov.',
        'maxlength':     'Heslo musí mať menej než 64 znakov.',
      },
      'confirmpassword':{
        'notequal':     'Heslá sa nezhodujú ',
      },
    };
    // resetovanie errorov
    this.formErrors = {
      'full_name': '',
      'email': '',
      'password': '',
      'confirmpassword': '',
    };
  }

  onSubmit() {
    this.user = this.modelForm.value;
    if (this.modelForm.valid){
      this.saveUser();
    }
  }

  saveUser(){
    if (this.user.created_at==null){
      this.userService.create(this.user).subscribe();
      this.loggerService.log('Create user %s', this.user);
    } else {
      this.userService.save(this.user).subscribe();
      this.loggerService.log('Save user %s', this.user);
    }
    // fixme: tento reload nie je dobry. Refresh objektov v tabulke by mal ist dynamicky bez nutnosti reloadnut stranku
    location.reload();

    this.flashMessagesService.show('POuzivatel bol uloženy!', { cssClass: 'alert-success', timeout: 3000 });

  }

  ngOnChanges(){

    if (this.user!=null){
      this.bTitle = "Upraviť používatela";
    } else {
      this.bTitle = "Vytvoriť používatela";
    }
  }

  onReset(){
    console.log('reset');
    this.user = new User();
    this.bTitle = "Vytvoriť používatela";
  }

}

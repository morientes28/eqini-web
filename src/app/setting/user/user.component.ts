import { Component, OnInit } from '@angular/core';
import {Source} from "../../core/datatable/datatable.model";
import {UnitType, UnitTypeEnum} from "../../shared/shared.enums";
import {LoggerService} from "../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {User} from "./user.model";
import {UserService} from "./user.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [ UserService ]
})
export class UserComponent implements OnInit {

  unit : UnitType = new UnitType(UnitTypeEnum.DESHBOARD);
  source: Source;

  editUser: User;

  select = (event: any) => {
    this.loggerService.log("on select", this);
    this.userService.get(event.id).subscribe(user => this.editUser = user);
  };

  delete = (event: any) => {
    this.loggerService.log('delete entity id: ' + event.id + ' from usercomponent');
    this.userService.drop( event.id).subscribe(() =>{
      this.refresh();
    });
    this.flashMessagesService.show('Pouzivatel bol vymazany!', { cssClass: 'alert-success', timeout: 3000 });
  };

  constructor(
    private userService: UserService,
    private loggerService: LoggerService,
    private flashMessagesService: FlashMessagesService)
  { }

  ngOnInit(){
    this.refresh();
  }

  refresh(){
    this.userService.getUsers().subscribe((users) => {
      this.source = this.userService.getSource(users);
    });
  }

}

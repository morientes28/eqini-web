import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {ProfileComponent} from "./profile/profile.component";
import {CompanyComponent} from "./company/company.component";
import {BranchComponent} from "./branch/branch.component";
import {PageNotFoundComponent} from "../core/page-not-found/page-not-found.component";
import {UserComponent} from "./user/user.component";
import {AuthGuard} from "../access/auth/auth.guard";


const settingRoutes: Routes = [
  {path: 'setting/profile', component: ProfileComponent, canActivate: [AuthGuard] },
  {path: 'setting/company', component: CompanyComponent, canActivate: [AuthGuard] },
  {path: 'setting/branches', component: BranchComponent, canActivate: [AuthGuard] },
  {path: 'setting/users', component: UserComponent, canActivate: [AuthGuard] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(settingRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class SettingRoutingModule {}

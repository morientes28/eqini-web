import {CompanyFormComponent} from "./company/form/company-form.component";
import {CompanyComponent} from "./company/company.component";
import {BranchFormComponent} from "./branch/form/branch-form.component";
import {DatatableComponent} from "../core/datatable/datatable.component";
import {BranchComponent} from "./branch/branch.component";
import {ProfileFormComponent} from "./profile/form/profile-form.component";
import {ProfileComponent} from "./profile/profile.component";
import {FormsModule} from "@angular/forms";
import {CoreModule} from "../core/core.module";
import {SettingRoutingModule} from "./setting-routing.module";
import {CommonModule} from "@angular/common";
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import { UserComponent } from './user/user.component';
import { UserFormComponent } from './user/form/user-form.component';

@NgModule({
  imports: [
    CommonModule,
    SettingRoutingModule,
    CoreModule,
    FormsModule,
  ],
  declarations: [
    ProfileComponent,
    ProfileFormComponent,
    BranchComponent,
    BranchFormComponent,
    CompanyComponent,
    CompanyFormComponent,
    UserComponent,
    UserFormComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})

export class SettingModule { }

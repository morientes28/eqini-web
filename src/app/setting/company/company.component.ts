import { Component, OnInit } from '@angular/core';
import {UnitType, UnitTypeEnum} from "../../shared/shared.enums";

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  unit : UnitType = new UnitType(UnitTypeEnum.DESHBOARD);
  constructor() { }

  ngOnInit() {
  }

}

import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";

import {SharedService} from "../../shared/shared.service";
import {LoggerService} from "../../core/logger.service";
import {environment} from "../../../environments/environment";
import {Company} from "./company";

@Injectable()
export class CompanyService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http) {
    super();
  }

  /**
   * ulozenie modeloveho objektu company
   * @param company
   */
  save(company:Company): Observable<Company> {
    this.logger.info('call branch service save', company);
    let result = this.http.put(`${this.api}/account/companies/${this.getCurrentUser().company}/`,
      company,
      {headers: this.getHeaders()})
      .map(mapCompany);

    return result;
  }

  /**
   * ziskanie modeloveho objektu Company podla domain
   * @param domain - jednoznacny identifikator objektu domain
   */
  get(domain: string): Observable<Company> {
    this.logger.info('call branch service get ', domain);

    let result = this.http.get(`${this.api}/account/companies/${domain}/`,
      {headers: this.getHeaders()})
      .map(mapCompany);

    return result;
  }

}

function mapCompany(response: Response): Company {
  return toCompany(response.json());
}

function toCompany(r: any): Company {
  return r;
}



import { Component, OnInit } from '@angular/core';
import {Company} from "../company";
import {ValidatedForm} from "../../../core/form/validated-form";
import {FlashMessagesService} from "angular2-flash-messages";
import {LoggerService} from "../../../core/logger.service";
import {CompanyService} from "../company.service";


@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.css'],
  providers: [ CompanyService ]
})
export class CompanyFormComponent extends ValidatedForm implements OnInit {

  submitted = false;
  company: Company;

  constructor(private companyService: CompanyService,
              private flashMessagesService: FlashMessagesService,
              private loggerService: LoggerService,) {
    super();
  }

  onSubmit() {
    console.log(this.formErrors);
    console.log(this.modelForm.valid);
    this.submitted = true;
    this.company = this.modelForm.value;
   // if (this.modelForm.valid){
      this.saveCompany();
   // }
  }

  saveCompany(){
    //todo: servis na ulozenie validneho profilu
    this.companyService.save(this.company).subscribe(()=>{
      this.flashMessagesService.show('Profil firmy bol uložený!', { cssClass: 'alert-success', timeout: 3000 });
    });
  }

  ngOnInit() {
    // default prazdny objekt
    this.company = new Company();

    // nacitanie dat o firme podla prihlaseneho usera resp. domeny firmy
    this.companyService.get(this.companyService.getCurrentUser().company).subscribe(
      (c) => {
        this.company = c;
        console.log(this.company);
      }
    );

    this.validationMessages = {
      'title': {
        'required':      'Položka Názov spoločnosti je povinná.',
        'maxlength':     'Položka Názov spoločnosti môže mať max. 50 znakov.',
      },
      'subname': {
        'maxlength':     'Položka Názov spoločnosti môže mať max. 50 znakov.',
      },
      'ico':{
        'required':      'Položka IČO je povinná.',
        'pattern':       'Zlý formát IČO (príklad: 12345678).',
        'maxlength':     'Maximálna dĺžka položky IČO je 8 znakov.',
      },
      'dic':{
        'required':      'Položka DIČ je povinná.',
        'pattern':       'Zlý formát DIČ (príklad: 1234567890).',
        'maxlength':     'Maximálna dĺžka položky IČO je 10 znakov.',
      },
      'icdph':{
        'pattern':       'Zlý formát IČ DPH (príklad: SK1234567890).',
        'maxlength':     'Maximálna dĺžka položky DIČ je 12 znakov.',
      },
      'base_street':{
        'maxlength':     'Maximálna dĺžka položky Ulica je 50 znakov.',
      },
      'base_city':{
          'maxlength':   'Maximálna dĺžka položky Ulica je 50 znakov.',
      },
      'base_zip':{
        'maxlength':     'Maximálna dĺžka položky je 10 znakov.',
      },
      'base_country':{
        'maxlength':     'Maximálna dĺžka položky je 30 znakov.',
      },
      'email_1':{
        'required':      'Položka E-mail je povinná.',
        'email':         'Nespravny format emailu',
      },
      'phone_1': {
        'maxlength':     'Maximálna dĺžka položky je 20 znakov.',
      },
      'phone_2': {
        'maxlength':     'Maximálna dĺžka položky je 20 znakov.',
      }
    };

    this.formErrors = {
      'title': '',
      'subname':'',
      'ico': '',
      'dic': '',
      'icdph': '',
      'email_1': '',
      'web': '',
      'base_street': '',
      'base_zip': '',
      'base_city': '',
      'base_country': '',
      'phone_1': '',
      'phone_2': '',
    };
  }

}

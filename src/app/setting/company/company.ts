import {Adress} from "../../shared/adress.model";

export class Company extends Adress{

  public title: string; // nazov firmy
  public subname: string; // doplnujuci nazov firmy (podruzny)
  public ico: string; // ICO spolocnosti
  public dic: string; // DIC spolocnosti
  public icdph: string; // IC DPH spolocnosti
  public is_dph: boolean; // platca DPH
  public web: string; // webova stranka spolocnosti
  public domain:string;

	constructor() {
	  super();
  }

}

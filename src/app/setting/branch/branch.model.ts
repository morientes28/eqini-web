import {Adress} from "../../shared/adress.model";

export class Branch extends Adress{

  constructor(
    public id: number = null,
    public title: string = null,
    public city: number = 0,
    public contact: string= null,

    public url: string = null,
    public slug: string = null,
    public web: string = null,
  ){
    super();
  }
}


import { Component, OnInit } from '@angular/core';
import { UnitType, UnitTypeEnum } from "../../shared/shared.enums";
import { Source } from "../../core/datatable/datatable.model";
import {Branch} from "./branch.model";
import {LoggerService} from "../../core/logger.service";
import {BranchService} from "./branch.service";
import {FlashMessagesService} from "angular2-flash-messages";

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css'],
  providers: [ BranchService ]
})
export class BranchComponent implements OnInit {

  unit : UnitType = new UnitType(UnitTypeEnum.DESHBOARD);
  source: Source;

  editBranch: Branch;


  select = (event: any) => {
    this.loggerService.log("on update", this);
    this.branchService.get(event.id).subscribe(branch => this.editBranch = branch);
  };

  delete = (event: any) => {
    this.loggerService.log('delete entity id: ' + event.id + ' from branchcomponent');
    this.branchService.drop( event.id).subscribe(() =>{
      this.refresh();
    });
    this.flashMessagesService.show('Pobočka bola vymazaná!', { cssClass: 'alert-success', timeout: 3000 });
  };

  constructor(
    private branchService: BranchService,
    private loggerService: LoggerService,
    private flashMessagesService: FlashMessagesService)
    { }

  ngOnInit(){
    this.refresh();
  }

  refresh(){
    this.branchService.getBranches().subscribe((branches) => {
      this.source = this.branchService.getSource(branches);
    });
  }



}

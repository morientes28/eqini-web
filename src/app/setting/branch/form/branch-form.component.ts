import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Branch} from "../branch.model";
import {ValidatedForm} from "../../../core/form/validated-form";
import {BranchService} from "../branch.service";
import {LoggerService} from "../../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";

@Component({
  selector: 'app-branch-form',
  templateUrl: './branch-form.component.html',
  styleUrls: ['./branch-form.component.css'],
  providers: [ BranchService ]
})
export class BranchFormComponent extends ValidatedForm implements OnInit, OnChanges {

  @Input() branch: Branch;
  public bTitle = "Vytvoriť pobočku";

  constructor(private branchService: BranchService,
              private loggerService: LoggerService,
              private flashMessagesService: FlashMessagesService) {
    super();
  }

  ngOnInit() {
    this.branch = new Branch();

    // nastavenie validacnych hlasok
    this.validationMessages = {
      'name': {
        'required':      'Názov pobočky je vyžadovaný.',
        'minlength':     'Názov pobočky musí mať viac ako 4 znaky.',
        'maxlength':     'Názov pobočky musí mať menej než 64 znakov.',
      },
      'city':{

      },
      'contact':{
        'minlength':     'Kontaktná osoba musí mať viac ako 4 znaky.',
        'maxlength':     'Kontaktná osoba musí mať menej než 64 znakov.',
      },
    };
    // resetovanie errorov
    this.formErrors = {
      'name': '',
      'city': '',
      'contact': '',
    };
  }

  onSubmit() {
    console.log(this.modelForm.value);
    this.branch = this.modelForm.value;
    if (this.modelForm.valid){
      this.saveBranch();
    }
  }

  saveBranch(){
    console.log(this.branch.created_at);
    if (this.branch.created_at==null){
      this.branchService.create(this.branch).subscribe();
      this.loggerService.log('Create branch %s', this.branch);
    } else {
      this.branchService.save(this.branch).subscribe();
      this.loggerService.log('Save branch %s', this.branch);
    }
    // fixme: tento reload nie je dobry. Refresh objektov v tabulke by mal ist dynamicky bez nutnosti reloadnut stranku
    location.reload();

    this.flashMessagesService.show('Pobočka bola uložená!', { cssClass: 'alert-success', timeout: 3000 });

  }

  ngOnChanges(){

    if (this.branch!=null){
      this.bTitle = "Upraviť pobočku";
    } else {
      this.bTitle = "Vytvoriť pobočku";
    }
  }

  onReset(){
    console.log('reset');
    this.branch = new Branch();
    this.bTitle = "Vytvoriť pobočku";
  }

}

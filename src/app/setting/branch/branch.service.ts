import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";

import {SharedService} from "../../shared/shared.service";
import {Branch} from "./branch.model";
import {LoggerService} from "../../core/logger.service";
import {Data, Source, Row, Action} from "../../core/datatable/datatable.model";

import {environment} from "../../../environments/environment";

@Injectable()
export class BranchService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http) {
    super();
  }

  /**
   * Ziskanie vsetkych pobociek
   * @returns {Observable<Branch>}
   */
  getBranches(): Observable<Branch[]>{

    this.logger.log('call branch service getSource');

    let result = this.http.get(`${this.api}/account/branches/`,
      {headers: this.getHeaders()})
      .map(mapBranchs);

    return result;

  }
  /**
   * Ziskanie source pre komponentu datatables na vykreslenie dat
   */
  getSource(branches:Branch[]): Source{

    let rows = [];
    let d:Data;
    for(let i=0; i<branches.length; i++){
      rows.push(new Row(branches[i].slug,
        [branches[i].title,
          "" ,
          branches[i].created_by['full_name'],
          branches[i].url,
          branches[i].slug,
          ""]));
    }

    d = new Data(
      ["Názov", "Mesto", "Kontaktná osoba", "URL", "Slug", "Akcie"],
      rows,
      new Action(true, true, false));
    return new Source("datatable", d);
  }
  /**
   * ulozenie modeloveho objektu branch
   * @param branch
   */
  save(branch:Branch): Observable<Branch> {
    this.logger.info('call branch service save', branch);
    let result = this.http.put(`${this.api}/account/branches/${branch.slug}/`,
      branch,
      {headers: this.getHeaders()})
      .map(mapBranch);

    return result;
  }

  create(branch:Branch): Observable<Branch> {
    this.logger.info('call branch service create', branch);
    let result = this.http.post(`${this.api}/account/branches/`,
      branch,
      {headers: this.getHeaders()})
      .map(mapBranch);

    return result;
  }
  /**
   * ziskanie modeloveho objektu Branch podla slugu
   * @param slug - jednoznacny identifikator objektu branch
   */
  get(slug: string): Observable<Branch> {
    this.logger.info('call branch service get ', slug);

    let result = this.http.get(`${this.api}/account/branches/${slug}/`,
      {headers: this.getHeaders()})
      .map(mapBranch);

    return result;
  }

  /**
   * mazanie modeloveho objektu
   * @param slug
   */
  drop(slug: string): Observable<boolean> {
    this.logger.info('call branch service drop', slug);
    let result = this.http.delete(`${this.api}/account/branches/${slug}/`,
      {headers: this.getHeaders()})
      .map(mapBoolean);

    return result;
  }
}

function mapBranchs(response: Response): Branch[] {
  return response.json().map(toBranch);
}

function mapBranch(response: Response): Branch {
  return toBranch(response.json());
}

function toBranch(r: any): Branch {
  return r;
}

function mapBoolean(response: Response): boolean {
  return response.json();
}


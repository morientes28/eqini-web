/**
 * zakladny model, z ktoreho sa extenduju dalsie modely
 */
export class Base {

  public created_at: string = null;
  public created_by: object;
  public deleted: boolean;
  public deleted_at: string;
  public deleted_by: object;
  public updated_at: string;
  public updated_by: object;
  public restored_at: string;
  public restored_by: object = null;

  constructor(
  ){}
}

import {Base} from "./base.model";
/**
 * zakladny model, z ktoreho sa extenduju dalsie modely
 */
export class Adress extends Base{

  public base_country: string = null;
  public base_state: string = null;
  public base_street: string = null;
  public base_zip: string = null;
  public email_1: string;
  public email_2: string;
  public phone_1: string;
  public phone_1_type: string;
  public phone_2: string = null;
  public phone_2_type: string;
  public phone_3: string;
  public phone_3_type: string;
  public post_city: string;
  public post_country: string;
  public post_state: string;
  public post_street: string;
  public post_zip: string;

  constructor(){
    super();
  }
}

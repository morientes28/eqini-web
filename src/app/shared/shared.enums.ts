/**
 * Definovanie unit typov pre cely system.
 * Unit typ je vlastne aplikacia napriklad sklad alebo podvojne uctovnictvo.
 * UnitType definuje nazov unitu, skratku unitu.
 */

export enum UnitTypeEnum {
  SKLAD,
  PODVOJNE_UCTOVNICTVO,
  DESHBOARD,
  CISELNIKY,
}

export class UnitType {

  enumValue : UnitTypeEnum;
  name : string;

  constructor(enumValue: UnitTypeEnum){
    this.enumValue = enumValue;
    this.name = this.getName(enumValue);
  }

  /**
   * Ziskanie nazvu unitu v SK tvare
   * @param unitType
   * @returns {string}
   */
  private getName(unitType:UnitTypeEnum):string {
    switch (unitType) {
      case UnitTypeEnum.SKLAD:
        return "Sklad";
      case UnitTypeEnum.PODVOJNE_UCTOVNICTVO:
        return "Podvojné účtovníctvo";
      case UnitTypeEnum.CISELNIKY:
        return "Číselníky";
      default:
        return "Pracovná plocha"
    }
  }

}

export class Section {
  constructor(public stub: string,
              public title: string,
              public extend: boolean = true
  ){}
}


export class CiselnikSection extends Section{

  constructor(public stub: string,
              public title: string,
              public extend: boolean,
              public url: string,
              public urlPrivate: string,
              public displayedColumns: string[],
  ){
    super(stub, title, extend);
  }
}

export class CiselnikSectionList {

  public static list ():Set<CiselnikSection> {
    return new Set(
      [
        new CiselnikSection(
          "staty",
          "Štáty",
          false,
          "shared/localization/states/",
          "lists/localization/states/",
          ['name', 'shortcut', 'currency','market', 'checkbox']),
        new CiselnikSection(
          "mesta",
          "Mestá",
          false,
          "shared/localization/cities/",
          "lists/localization/cities/",
          []),
        new CiselnikSection(
          "krajiny",
          "Krajiny",
          false,
          "shared/localization/countries/",
          "lists/localization/countries/",
          []),
        new CiselnikSection(
          "meny",
          "Platobné meny",
          true,
          "shared/localization/currencies/",
          "lists/localization/currencies/",
          []),
        new CiselnikSection(
          "jazyky",
          "Jazyky",
          true,
          "shared/localization/languages/",
          "lists/localization/languages/",
        []),
        new CiselnikSection(
          "ucty-statusy",
          "Status účov",
          true,
          "shared/directory/account-statuses/",
          "lists/directory/account-statuses/",
        []),
        new CiselnikSection(
          "telefon-typy",
          "Typy telefónov",
          true,
          "shared/directory/phone-types/",
          "lists/directory/phone-types/",
          []),
        new CiselnikSection(
          "priority",
          "Priority",
          true,
          "shared/directory/priorities/",
          "lists/directory/priorities/",
          []),
        new CiselnikSection(
          "merne-jednotky",
          "Merne jednotky",
          true,
          "shared/warehouse/measure-units/",
          "lists/warehouse/measure-units/",
          ['name', 'shortcut', 'currency','market', 'checkbox']),
        new CiselnikSection(
          "banky",
          "Zoznam bánk",
          true,
          "shared/finance/banks/",
          "lists/finance/banks/",
          [])
      ]
    );
  };

  static valueOf(value: string):Section {

    // default hlavna sekcia
    let result = CiselnikSectionList.list()[0];
    CiselnikSectionList.list().forEach(function(item) {
      if (item.stub.valueOf() == value.valueOf()) {
        result = item;
      }
    });

    return result;
  }

  static valueOfCiselnikSection(value: string):CiselnikSection {

    // default hlavna sekcia
    let result = CiselnikSectionList.list()[0];
    CiselnikSectionList.list().forEach(function(item) {
      if (item.stub.valueOf() == value.valueOf()) {
        result = item;
      }
    });

    return result;
  }
}




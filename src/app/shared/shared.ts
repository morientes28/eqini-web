import {Branch} from "../setting/branch/branch.model";
/**
 * Prave prihlaseny pouzivatel ulozeny v cookies
 */
export class CurrentUser {

  constructor(
    public token: string,
    public email: string,
    public company: string){}  //domain
}

/**
 * Session data prihlaseneho pouzivatela
 */
export class CurrentData {

  constructor(
    // konretna brancha, nad ktorou sa robi
    public branch: Branch,
    // konkretna perioda (2017...)
    public period: string,
    // nazov firmy
    public companyName: string
  ) {}
}

import {Headers} from "@angular/http";
import {CurrentUser, CurrentData} from "./shared";
import {Branch} from "../setting/branch/branch.model";

export class SharedService{

  constructor()
  {  }



  getHeaders() {
    let headers = SharedService.createHeaders();
    headers.append('Authorization', this.getAuthToken());
    return headers;
  }

  getHeadersWithoutAuthorization() {
    return SharedService.createHeaders();
  }

  getAuthToken(){
    let storedToken:string = this.getCurrentUser().token;
    if(!storedToken) throw 'no token found';
    return storedToken;
  }

  getCurrentUser(){
    let storedUser:CurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(!storedUser) throw 'no current user found';
    return storedUser;
  }

  getCurrentData(){
    let storedData:CurrentData = JSON.parse(localStorage.getItem('currentData'));
    if(!storedData) throw 'no current data found';
    return storedData;
  }

  updateCurrentUser(email:string){
    let user = this.getCurrentUser();
    user.email = email;
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  updateCurrentData(branch:Branch, period:string){
    let data = this.getCurrentData();
    data.branch = branch;
    data.period = period;
    localStorage.setItem('currentData', JSON.stringify(data));
  }

  private static createHeaders(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin','*');
    return headers;
  }

}

import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {DashboardComponent} from "./units/dashboard/dashboard.component";
import {AuthGuard} from "./access/auth/auth.guard";

// import { AppComponent } from "./app.component";

const appRoutes: Routes = [

  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}

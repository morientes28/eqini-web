import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule   } from '@angular/forms';
import { HttpModule } from "@angular/http";

import { AppComponent } from './app.component';

import { AccessModule } from "./access/access.module";
import { AppRoutingModule } from "./app-routing.module";
import { PodUctoCoreModule } from "./units/pod-ucto/pod-ucto-core/pod-ucto-core.module";
import { SkladCoreModule } from "./units/sklad/sklad-core/sklad-core.module";
import { CoreModule } from "./core/core.module";
import { DashboardModule } from "./units/dashboard/dashboard.module";
import { SettingModule } from "./setting/setting.module";
import { FlashMessagesModule } from "angular2-flash-messages";
import { CiselnikyModule } from "./units/ciselniky/ciselniky.module";
import { SkladCiselnikyComponent } from './units/sklad/sklad-ciselniky/sklad-ciselniky.component';

import {MATERIAL_COMPATIBILITY_MODE} from '@angular/material';


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    AccessModule,
    PodUctoCoreModule,
    SkladCoreModule,
    CiselnikyModule,
    DashboardModule,
    SettingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    FlashMessagesModule
  ],
  declarations: [
    AppComponent,
    SkladCiselnikyComponent,
  ],
  exports: [
    RouterModule
  ],
  providers: [
    {provide: MATERIAL_COMPATIBILITY_MODE, useValue: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

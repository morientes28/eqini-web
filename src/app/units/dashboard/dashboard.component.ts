import {Component, OnInit, Output} from '@angular/core';
import {UnitTypeEnum, UnitType} from "../../shared/shared.enums";
import {Unit} from "../unit";
import {BranchService} from "../../setting/branch/branch.service";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ BranchService ]
})
export class DashboardComponent extends Unit implements OnInit {

  @Output() isFullScreen: boolean = false;

  constructor(public branchService: BranchService) {
    super(new UnitType(UnitTypeEnum.DESHBOARD), branchService);
  }

  ngOnInit() {

  }

}

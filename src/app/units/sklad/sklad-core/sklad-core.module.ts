import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SkladCoreRoutingModule} from "./sklad-core-routing.module";
import {SkladCoreComponent} from "./sklad-core.component";
import {CoreModule} from "../../../core/core.module";
import {CiselnikyModule} from "../../ciselniky/ciselniky.module";

@NgModule({
  imports: [
    CommonModule,
    SkladCoreRoutingModule,
    CoreModule,
    CiselnikyModule
  ],
  declarations: [
    SkladCoreComponent,
  ]
})
export class SkladCoreModule { }

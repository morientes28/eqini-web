import { Component, OnInit } from '@angular/core';
import {UnitType, UnitTypeEnum} from "../../../shared/shared.enums";
import {Unit} from "../../unit";
import {BranchService} from "../../../setting/branch/branch.service";

@Component({
  selector: 'app-sklad-core',
  templateUrl: './sklad-core.component.html',
  styleUrls: ['./sklad-core.component.css'],
  providers: [ BranchService ]
})
export class SkladCoreComponent extends Unit implements OnInit {

  constructor(public branchService:BranchService) {
    super(new UnitType(UnitTypeEnum.SKLAD), branchService);
  }

  ngOnInit() {


  }

}

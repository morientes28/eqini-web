import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { SkladCoreComponent} from "./sklad-core.component";
import {AuthGuard} from "../../../access/auth/auth.guard";
import {SkladCiselnikyComponent} from "../sklad-ciselniky/sklad-ciselniky.component";


const skladCoreRoutes: Routes = [
  {path: 'sklad', component: SkladCoreComponent, canActivate: [AuthGuard] },
  {path: 'sklad/ciselniky/:id', component: SkladCiselnikyComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(skladCoreRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SkladCoreRoutingModule {}

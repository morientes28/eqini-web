import { Component, OnInit } from '@angular/core';
import {Unit} from "../../unit";
import {
  UnitType, UnitTypeEnum,
  CiselnikSectionList
} from "../../../shared/shared.enums";
import {BranchService} from "../../../setting/branch/branch.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sklad-ciselniky',
  templateUrl: './sklad-ciselniky.component.html',
  styleUrls: ['./sklad-ciselniky.component.css'],
  providers: [ BranchService ]
})
export class SkladCiselnikyComponent extends Unit implements OnInit  {

  private sub: any;

  constructor(
    public branchService:BranchService,
    private route: ActivatedRoute) {

    super(new UnitType(UnitTypeEnum.SKLAD), branchService);

    this.sub = this.route.params.subscribe(params => {
      this.section = CiselnikSectionList.valueOf(params['id']);
    });
  }

  ngOnInit() {

    this.branchService.getBranches().subscribe((branches) => {
      this.branches = branches;
    });

    let currentData = this.branchService.getCurrentData();
    this.company = "Jednota s.r.o.";//currentData.companyName;
    this.period = currentData.period;
    this.branch = currentData.branch;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}

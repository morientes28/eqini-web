import {UnitType, Section} from "../shared/shared.enums";
import {Branch} from "../setting/branch/branch.model";
import {BranchService} from "../setting/branch/branch.service";
import {Output} from "@angular/core";

/**
 * Trieda, ktora definuje unitu v aplikacii. Unita je inak povedane
 * aplikacny modul ako napriklad sklad, ciselniky alebo podvojne uctovnictvo
 * Unitu jednoznacne identifikuje unitType.
 * Unita moze obsahovat rozne sekcie, ktore su napriklad pri ciselnikovej unite
 * Sekcia typu platby, sekcia statov, mesta ...
 */
export class Unit {

  @Output() section: Section;
  period: string;
  branch: Branch;
  branches: Branch[] = [];
  company: string;

  constructor(public unit:UnitType,
              public branchService:BranchService){

    this.period = (new Date()).getFullYear().toString();
    this.branchService.getBranches().subscribe((branches) => {
      this.branches = branches;
    });

    let currentData = this.branchService.getCurrentData();
    this.company = "Jednota s.r.o.";//currentData.companyName;
    this.period = currentData.period;
    this.branch = currentData.branch;
  }
}

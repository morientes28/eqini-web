import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {CiselnikyCoreComponent} from "./ciselniky-core/ciselniky-core.component";
import {AuthGuard} from "../../access/auth/auth.guard";

const ciselnikyRoutes: Routes = [
  {path: 'ciselniky', component: CiselnikyCoreComponent , canActivate: [AuthGuard] },
  {path: 'ciselniky/:id', component: CiselnikyCoreComponent , canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(ciselnikyRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CiselnikyRoutingModule {}

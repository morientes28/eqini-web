import {Component, OnInit, Input} from '@angular/core';
import {CiselnikSection, CiselnikSectionList, Section} from "../../../shared/shared.enums";
import {LoggerService} from "../../../core/logger.service";
import {CiselnikyService} from "../ciselniky.service";
import {Multibox, Ciselnik} from "../ciselnik.model";
declare var $ :any;

@Component({
  selector: 'app-ciselnik-multibox',
  templateUrl: './ciselnik-multibox.component.html',
  styleUrls: ['./ciselnik-multibox.component.css'],
  providers: [ CiselnikyService ]
})
export class CiselnikMultiboxComponent implements OnInit {

  multibox: Multibox;
  mA: Ciselnik[] = [];
  mC: Ciselnik[] = [];

  @Input() section:CiselnikSection;

  constructor(private service: CiselnikyService,
              private loggerService: LoggerService) { }

  ngOnInit() {

    this.initMultiboxData(this.section);

  }

  initMultiboxData(section: Section) {

    if (this.section!=null && this.section.url != null) {
      let cs: CiselnikSection = CiselnikSectionList.valueOfCiselnikSection(section.stub);

      this.service.getAllByCompany(cs).subscribe((ciselniky) => {
        this.mA = ciselniky;

        this.service.getAll(cs).subscribe((ciselniky) => {
          this.mC = this.join(this.mA, ciselniky);

          for(let i=0;i<this.mC.length; i++){
            for(let j=0;j<this.mA.length; j++)
              if (this.mA[j].id==this.mC[i].id) {
                this.mC[i].choosen = true;
              }
          }
          this.multibox = new Multibox("multibox", this.mC );

        });

      });

    }
  }

  private join(a, b) {
    let tmp: Ciselnik[] = [];
    let result: Ciselnik[] = [];
    for(let i=0;i<a.length; i++) {
      tmp[a[i].id] = a[i];
    }
    for(let j=0;j<b.length; j++){
      tmp[b[j].id] = b[j];
    }

    for(let prop in tmp){
      result.push(tmp[prop]);
    }
    return result;
  }

  submit(){

    let tmp = [];
    let array : Ciselnik[] = [];
    let selector = "#" + this.multibox.id + "  option:selected";


    selector = ".ms-elem-selection.ms-selected";
    $(selector).each(function() {
      tmp.push($(this).text().trim());
    });

    for (let i=0; i<tmp.length; i++){
      for (let j=0; j<this.multibox.source.length; j++){
        if (this.multibox.source[j].title==tmp[i]) {
          array.push(this.multibox.source[j]);
        }
      }
    }
    this.loggerService.info('submit values ');
    this.service.saveMultivalue(array, this.section);
  }

}

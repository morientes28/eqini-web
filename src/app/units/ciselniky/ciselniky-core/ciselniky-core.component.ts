import {Component, OnInit, Input} from '@angular/core';
import {
   CiselnikSection
} from "../../../shared/shared.enums";

@Component({
  selector: 'app-ciselniky-core',
  templateUrl: './ciselniky-core.component.html',
  styleUrls: ['./ciselniky-core.component.css']
})
export class CiselnikyCoreComponent implements OnInit {

  @Input() section:CiselnikSection;

  constructor() {

  }


  ngOnInit() {

  }



}

import {Component, OnInit, OnChanges, Input} from '@angular/core';
import {Ciselnik} from "../ciselnik.model";
import {ValidatedForm} from "../../../core/form/validated-form";
import {LoggerService} from "../../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {CiselnikyService} from "../ciselniky.service";
import {CiselnikSection} from "../../../shared/shared.enums";

@Component({
  selector: 'app-ciselnik-form',
  templateUrl: './ciselnik-form.component.html',
  styleUrls: ['./ciselnik-form.component.css'],
  providers: [ CiselnikyService ]
})
export class CiselnikFormComponent extends ValidatedForm implements OnInit, OnChanges {

  @Input() ciselnik : Ciselnik;
  @Input() section:CiselnikSection;

  bTitle: string = "Vytvoriť položku";

  constructor(private service: CiselnikyService,
              private logger: LoggerService,
              private flashMessagesService: FlashMessagesService) {
    super();
  }

  ngOnInit() {

    this.ciselnik = new Ciselnik();

    this.validationMessages = {
      'title': {
        'required':      'Položka je vyžadovaná.',
        'maxlength':     'Položka musí mať menej než 64 znakov.',
      },
      'description':{
        'maxlength':     'Položka musí mať menej než 128 znakov.',
      }
    };

    this.formErrors = {
      'title': '',
      'description': '',
    };
  }

  ngOnChanges(){

    if (this.ciselnik!=null){
      this.bTitle = "Upraviť položku";
    } else {
      this.bTitle = "Vytvoriť položku";
    }
  }

  onSubmit() {
    this.ciselnik = this.modelForm.value;
    if (this.modelForm.valid){
      this.save(this.ciselnik);
    }
  }

  save(ciselnik: Ciselnik){
    this.logger.log('save ciselnik '+ ciselnik);
    this.service.save(ciselnik, this.section).subscribe(()=>{
      this.flashMessagesService.show('Ciselník bol uloženy!', { cssClass: 'alert-success', timeout: 3000 });
    });

  }

}

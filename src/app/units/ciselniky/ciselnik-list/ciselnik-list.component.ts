import {Component, OnInit, Input} from '@angular/core';
import {Source2, Row} from "../../../core/datatable/datatable.model";
import {LoggerService} from "../../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";
import {CiselnikyService} from "../ciselniky.service";
import {Ciselnik} from "../ciselnik.model";
import {CiselnikSection} from "../../../shared/shared.enums";
import {Subject} from "rxjs";

@Component({
  selector: 'app-ciselnik-list',
  templateUrl: './ciselnik-list.component.html',
  styleUrls: ['./ciselnik-list.component.css'],
  providers:[CiselnikyService]
})
export class CiselnikListComponent implements OnInit {

  source: Source2;
  ciselnik: Ciselnik;
  bTitle: string;
  @Input() section:CiselnikSection;
  public static updateStatus: Subject<boolean> = new Subject();

  constructor(
              private ciselnikyService: CiselnikyService,
              private loggerService: LoggerService,
              private flashMessagesService: FlashMessagesService) {

    CiselnikListComponent.updateStatus.subscribe(res => {
      console.log(this.section);
      this.refresh();
    });
  }

  select = (event: any) => {
    this.loggerService.log("on select " + event.id, this);

    if (event.data[4]=='1') {
      this.ciselnikyService.delete(event.id, this.section).subscribe();
    } else {

      // localne
      if (event.data[3]=='centrálne'){
        console.log('lokalne');
        this.ciselnikyService.save(new Ciselnik(
            event.id,
            "",
            event.data[0],
            event.data[1],
            "",
            false,
            false), this.section).subscribe();
      }
    }

  };

  delete = (event: any) => {
    this.loggerService.log('delete entity id: ' + event.id + ' from CiselnikListComponent');
    this.ciselnikyService.delete(event.id, this.section).subscribe(()=>{
      this.refresh();
    });
    this.flashMessagesService.show('Položka bola vymazaná!', { cssClass: 'alert-success', timeout: 3000 });
    this.refresh();
  };

  update = (event: any) => {
    this.loggerService.log('update entity id: ' + event.id + ' from CiselnikListComponent');
    this.ciselnikyService.get(event.id, this.section).subscribe((ciselnik) => {
      this.ciselnik = ciselnik;
    });
    this.bTitle = "Upraviť";
  };

  onSubmit() {
    this.loggerService.log('save entity id: ' + this.ciselnik + ' from CiselnikListComponent');

    if (this.ciselnik.id == '0') {

      this.ciselnikyService.save(this.ciselnik, this.section).subscribe(() => {
        this.refresh();
      });

    } else {

      this.ciselnikyService.update(this.ciselnik, this.section).subscribe(() => {
        this.refresh();
      });
    }
    this.flashMessagesService.show('Položka bola uložená!', { cssClass: 'alert-success', timeout: 3000 });

  }

  ngOnInit(){
    this.refresh();
  }

  clear(){
    this.ciselnik = new Ciselnik("", "", "", "", "", false, false);
    this.bTitle = "Vytvoriť";
  }

  refresh(){
    console.log(this.section);
    if (this.section!=null && this.section.url != null) {
       this.ciselnikyService.getAllByCompany(this.section).subscribe((ciselnik) => {

         // lokalne
         this.source = this.ciselnikyService.getSource(ciselnik);
         let A = this.source.data.rows;


         this.ciselnikyService.getAll(this.section).subscribe((ciselnik) => {

           // centralne

           let B = this.ciselnikyService.getSource(ciselnik).data.rows;

           // prienik
           this.source.data.rows = this.prienik(A, B);

         });
       });
      this.clear();
    }
  }

  prienik(A:Row[], B:Row[]) : Row[]{


    for (let a of A){
      a.data[3] = 'lokálne';
      a.data[4] = '1';
      for (let b of B){
        if (a.data[0] == b.data[0]){
          a.data[3] = 'centrálne';
          a.data[4] = '1';
        }
      }
    }
    let C:Row[] = [];
    for (let b of B){
      let insert = true;
      for (let a of A){
        if (b.data[0]==a.data[0]) {
          insert = false;
          break;
        }
      }
      if (insert){
        b.data[3] = 'centrálne';
        b.data[4] = '0';
        C.push(b);
      }
    }

    C = C.concat(A);

    return C;
  }

  onReset(){
    this.clear();
  }
}

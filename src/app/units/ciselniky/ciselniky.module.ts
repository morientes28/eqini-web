import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CiselnikyRoutingModule} from "./ciselniky-routing.module";
import {CiselnikyCoreComponent} from "./ciselniky-core/ciselniky-core.component";
import {CoreModule} from "../../core/core.module";
import { CiselnikHeaderComponent } from './ciselnik-header/ciselnik-header.component';
import { CiselnikListComponent } from './ciselnik-list/ciselnik-list.component';
import { CiselnikFormComponent } from './ciselnik-form/ciselnik-form.component';
import {FormsModule} from "@angular/forms";
import { CiselnikMultiboxComponent } from './ciselnik-multibox/ciselnik-multibox.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    CiselnikyRoutingModule,
    FormsModule,
  ],
  declarations: [
    CiselnikyCoreComponent,
    CiselnikHeaderComponent,
    CiselnikListComponent,
    CiselnikFormComponent,
    CiselnikMultiboxComponent
  ],
  exports: [
    CiselnikyCoreComponent,
  ]
})
export class CiselnikyModule { }

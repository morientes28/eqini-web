import {Component, OnInit, Input} from '@angular/core';
import { CiselnikSection} from "../../../shared/shared.enums";

@Component({
  selector: 'app-ciselnik-header',
  templateUrl: './ciselnik-header.component.html',
  styleUrls: ['./ciselnik-header.component.css']
})
export class CiselnikHeaderComponent implements OnInit {

  @Input() section:CiselnikSection;

  constructor() { }

  ngOnInit() {
    console.log(this.section);
  }

}

import {environment} from "../../../environments/environment";
import {Injectable} from "@angular/core";
import {SharedService} from "../../shared/shared.service";
import {LoggerService} from "../../core/logger.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Source2, Data, Row, Action, Data2} from "../../core/datatable/datatable.model";
import {Ciselnik, Multibox} from "./ciselnik.model";
import {Section, CiselnikSection} from "../../shared/shared.enums";

@Injectable()
export class CiselnikyService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http) {
    super();
  }

  /**
   * Ziskanie source pre komponentu datatables na vykreslenie dat
   */
  getSource(ciselnik:Ciselnik[]): Source2{

    let rows = [];
    let d:Data2;
    let cdb;
    let choosen:boolean = false;
    for(let i=0; i<ciselnik.length; i++){

      cdb = ciselnik[i].cdb ? "centrálne" : "lokálne";
      choosen = ciselnik[i].choosen;
      choosen = true;
      rows.push(new Row(
        ciselnik[i].id.toString(),
                    [
                      ciselnik[i].title,
                      ciselnik[i].abrv,
                      ciselnik[i].type,
                      cdb,
                      choosen
                    ])
      );
    }

    d = new Data2(
      ["Názov","Skratka","Úložisko","Priradený","Akcie"],
      rows);
    return new Source2("datatable", d);
  }

  getAll(section:CiselnikSection):Observable<Ciselnik[]>{

    return this.http.get(
      this.getCiselnikUrl(section),
      {headers: this.getHeaders()})
      .map(mapCiselniks);

  }

  getAllByCompany(section:CiselnikSection):Observable<Ciselnik[]>{
    return this.http.get(
      this.getCiselnikPrivateUrl(section),
      {headers: this.getHeaders()})
      .map(mapCiselniks);
  }

  /**
   * Vyber GET url pre konkretny ciselnik
   * @param section
   * @returns {string}
   */
  getCiselnikUrl(section:CiselnikSection){
    return `${this.api}/${section.url}`;
  }
  getCiselnikPrivateUrl(section:CiselnikSection){
    return `${this.api}/${section.urlPrivate}`;
  }

  get(id:string, section:CiselnikSection):Observable<Ciselnik>{
    this.logger.info('call ciselnik service get', id);
    let url = this.getCiselnikUrl(section);
    let result = this.http.get(
      url + id + "/",
       {headers: this.getHeaders()})
       .map(mapCiselnik);

   return result;

  }

  getPrivate(id:string, section:CiselnikSection):Observable<Ciselnik>{
    this.logger.info('call ciselnik service get', id);
    let url = this.getCiselnikUrl(section).replace("shared", "lists");
    let result = this.http.get(
      url + id + "/",
      {headers: this.getHeaders()})
      .map(mapCiselnik);

    return result;

  }


  save(ciselnik: Ciselnik, section:CiselnikSection):Observable<Ciselnik>{
    this.logger.info('call ciselnik service save', ciselnik);
    let url = this.getCiselnikUrl(section).replace("shared", "lists");
    let result = this.http.post(
      url,
      ciselnik,
      {headers: this.getHeaders()})
      .map(mapCiselnik);

    return result;
  }

  update(ciselnik: Ciselnik, section:CiselnikSection):Observable<Ciselnik>{
    this.logger.info('call ciselnik service update', ciselnik);
    let url = this.getCiselnikUrl(section).replace("shared", "lists");
    let result = this.http.put(
      url + ciselnik.id + "/",
      ciselnik,
      {headers: this.getHeaders()})
      .map(mapCiselnik);

    return result;
  }

  delete(id: string, section:CiselnikSection):Observable<Ciselnik>{
    this.logger.info('call ciselnik service delete', id);
    let url = this.getCiselnikUrl(section).replace("shared", "lists");
    let result = this.http.delete(
      url + id + "/",
      {headers: this.getHeaders()})
      .map(mapCiselnik);

    return result;
  }

  add(ciselnik: Ciselnik[], section:CiselnikSection):Observable<boolean>{
    this.logger.info('call ciselnik service add', ciselnik);
    //fixme: url z ciselnika
    let url = `${this.api}/lists/localization/states/add/`;
    let result = this.http.post(
      url,
      ciselnik,
      {headers: this.getHeaders()})
      .map(mapBoolean);

    return result;
  }

  saveMultivalue(ciselniky: Ciselnik[], section:CiselnikSection){
    this.add(ciselniky, section).subscribe();
    // this.dropAll(section).subscribe(()=>{
    //   for(let i=0;i<ciselniky.length; i++){
    //     this.save(ciselniky[i], section).subscribe();
    //   }
    // });

  }

  dropAll(section:CiselnikSection):Observable<boolean>{
    let url = this.getCiselnikUrl(section).replace("shared", "lists")+"delete/";
    let result = this.http.delete(
      url,
      {headers: this.getHeaders()})
      .map(mapBoolean);

    return result;
  }
}

function mapCiselniks(response: Response): Ciselnik[] {
  return response.json().map(toCiselnik);
}

function mapCiselnik(response: Response): Ciselnik {
  return toCiselnik(response.json());
}

function toCiselnik(r: any): Ciselnik {
  return r;
}

function mapBoolean(response: Response): boolean {
  return response.json();
}

import {Base} from "../../shared/base.model";

export class Ciselnik extends Base{

  constructor(  public id: string = null,
                public slug: string = null,
                public title: string = null,
                public abrv: string = null,
                public type: string = null,
                public cdb: boolean = true,
                public choosen: boolean = false,
                public currency: string = null){
    super();
  }
}

export class Multibox {

  constructor(public id: string ="multibox",
              public source: Ciselnik[])
  { }
}

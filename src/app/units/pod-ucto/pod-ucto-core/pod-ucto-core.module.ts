import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StavPokladneComponent } from '../stav-pokladne/stav-pokladne.component';
import { PodUctoCoreComponent } from './pod-ucto-core.component';
import { PodUctoCoreRoutingModule } from "./pod-ucto-core-routing.module";
import {CoreModule} from "../../../core/core.module";

@NgModule({
  imports: [
    CommonModule,
    PodUctoCoreRoutingModule,
    CoreModule
  ],
  declarations: [
    StavPokladneComponent,
    PodUctoCoreComponent,
  ]
})
export class PodUctoCoreModule { }

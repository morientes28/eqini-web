import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import {PodUctoCoreComponent} from "./pod-ucto-core.component";
import {AuthGuard} from "../../../access/auth/auth.guard";


const podUctoRoutes: Routes = [
  {path: 'podvojne-uctovnictvo', component: PodUctoCoreComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(podUctoRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PodUctoCoreRoutingModule {}

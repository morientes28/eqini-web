import { Component, OnInit } from '@angular/core';
import {UnitType, UnitTypeEnum} from "../../../shared/shared.enums";
import {Unit} from "../../unit";
import {BranchService} from "../../../setting/branch/branch.service";

@Component({
  selector: 'app-pod-ucto-core',
  templateUrl: './pod-ucto-core.component.html',
  styleUrls: ['./pod-ucto-core.component.css'],
  providers: [ BranchService ]
})
export class PodUctoCoreComponent extends Unit implements OnInit {

  constructor(public branchService:BranchService) {
    super(new UnitType(UnitTypeEnum.PODVOJNE_UCTOVNICTVO), branchService);
  }

  ngOnInit() {
  }

}

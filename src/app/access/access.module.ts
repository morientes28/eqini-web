import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EqualValidator} from "./registration/equal-validator.directive";  // import validator

import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {AccessRoutingModule} from "./access-routing.module";
import {FormsModule} from "@angular/forms";
import {AuthGuard} from "./auth/auth.guard";
import {AuthenticationService} from "./auth/authentication.service";

// used to create fake backend
import {fakeBackendProvider} from "./auth/fake-backend";
import {MockBackend} from "@angular/http/testing";
import {BaseRequestOptions} from "@angular/http";

@NgModule({
  imports: [
    CommonModule,
    AccessRoutingModule,
    FormsModule,
  ],
  declarations: [
    LoginComponent,
    RegistrationComponent,
    EqualValidator,
  ],
  providers: [
    AuthGuard,
    AuthenticationService,

    // providers used to create fake backend
   // fakeBackendProvider,
   // MockBackend,
   // BaseRequestOptions
  ]
})
export class AccessModule { }

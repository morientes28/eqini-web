export class Company {
    title: string; // nazov spolocnosti - musi byt zadane
    domain: string; // domena - nemusi byt zadana
    full_name: string; // cele meno - musi byt zadane
    email: string; // musi byt zadany a validny
    password: string; // musi byt zadane - minimalne 6 znakov
    confirmpassword: string; // musi byt zadane, zhodovat sa s heslom
}

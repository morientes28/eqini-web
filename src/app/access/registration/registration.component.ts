import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Company } from '../company';
import { AccessService } from "../access.service";
import {ValidatedForm} from "../../core/form/validated-form";
import {LoggerService} from "../../core/logger.service";
import {FlashMessagesService} from "angular2-flash-messages";

declare function require(moduleName: string): any;
const { version: appVersion } = require('../../../../package.json')

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [ AccessService ]
})
export class RegistrationComponent extends ValidatedForm implements OnInit {

  public company: Company;

  appVersion : string;

  constructor(
    private accessService: AccessService,
    private route: Router,
    private loggerService: LoggerService,
    private flashMessagesService: FlashMessagesService) {
    super();
  }

  ngOnInit(){
    this.company = {
      title: '',
      domain: '',
      full_name: '',
      email: '',
      password: '',
      confirmpassword: '',
    };

    this.appVersion = appVersion;
    this.validationMessages = {
      'title': {
        'required':      'Názov pobočky je vyžadovaný.',
      },
      'domain':{

      },
      'full_name':{

      },
      'email':{

      },
      'password':{

      },
      'confirmpassword':{

      },
    };
    // resetovanie errorov
    this.formErrors = {
      'title': '',
      'domain': '',
      'full_name': '',
      'email': '',
      'password': '',
      'confirmpassword': '',
    };
  }

  onSubmit() {
    console.log(this.modelForm);
    this.company = this.modelForm.value;
    if (this.modelForm.valid) {
      this.accessService.domainNotExist(this.company).subscribe((result) => {
        if (result) {
          this.accessService.emailNotExist(this.company).subscribe((result) => {
            if (result) {
              this.accessService.registerCompany(this.company).subscribe((result) => {

                this.accessService.registerUser(this.company).subscribe((result) => {
                  // todo nejakey progress bar
                  this.route.navigate(['/dashboard']);
                });
              });
            } else {
              this.flashMessagesService.show('Email uz existuje!', { cssClass: 'alert-danger', timeout: 3000 });
            }
          });
        } else {
          this.flashMessagesService.show('Firma uz existuje!', { cssClass: 'alert-danger', timeout: 3000 });
        }
      });
    }

  }

}

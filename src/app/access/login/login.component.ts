import { Component, OnInit } from '@angular/core';
declare function require(moduleName: string): any;
const { version: appVersion } = require('../../../../package.json')

import {Router} from "@angular/router";
import {AuthenticationService} from "../auth/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  appVersion : string;

  model: any = {};
  loading = false;
  error = '';

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.appVersion = appVersion;
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        if (result === true) {
          // login successful
          this.router.navigate(['/']);
        }
      }, error=>{
        this.error = 'Prihlasovacie meno alebo heslo je nesprávne';
        this.loading = false;
      });
  }

}

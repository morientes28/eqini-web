import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {LoggerService} from "../core/logger.service";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';

import {Company} from "./company";
import {SharedService} from "../shared/shared.service";

@Injectable()
export class AccessService extends SharedService{

  private api: string = environment.backendApiUrl;

  constructor(
    private logger: LoggerService,
    private http: Http) {
    super();
  }

  domainNotExist(company: Company): Observable<boolean> {
    this.logger.log('check domain '+ company.domain);
    let result = this.http.get(`${this.api}/account/register/check/domain/${company.domain}/`,
      {headers: this.getHeadersWithoutAuthorization()});
    return result.map(mapStatus);
  }

  emailNotExist(company: Company):Observable<boolean> {
    this.logger.log('check email '+ company.email);
    let result = this.http.get(`${this.api}/account/register/check/email/${company.email}/`,
      {headers: this.getHeadersWithoutAuthorization()});
    return result.map(mapStatus);
  }

  registerCompany(company: Company): Observable<Company> {
    this.logger.log('register '+ company.full_name);

    let result = this.http.post(`${this.api}/account/register/company/`,
      company,
      {headers: this.getHeadersWithoutAuthorization()}).map(mapCompany);

    this.logger.dump(result);
    return result;
  }

  registerUser(company: Company): Observable<Company> {
    this.logger.log('register '+ company.email);

    let result = this.http.post(`${this.api}/account/register/users/`,
      company,
      {headers: this.getHeadersWithoutAuthorization()}).map(mapCompany);

    this.logger.dump(result);
    return result;
  }

}

function mapCompany(response: Response): Company {
  return toCompany(response.json());
}

function toCompany(r: any): Company {
  return r;
}

function mapStatus(response: Response): boolean {
  if (response.status==206)
    return false
  return true;
}

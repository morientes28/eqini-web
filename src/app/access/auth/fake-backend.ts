﻿import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

/**
 * !!! NEPUZIVA SA !!!
 *
 * ostalo len ako priklad ako sa da namockovat
 * volanie http vrstvy
 *
 * @param backend
 * @param options
 * @returns {Http}
 */
export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
        let testUser = { username: 'test', password: 'test' };

        // wrap in timeout to simulate server api call
        setTimeout(() => {

            // fake authenticate api end point
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                // get parameters from post request
                let params = JSON.parse(connection.request.getBody());

                // check user credentials and return fake jwt token if valid
                if (params.username === testUser.username && params.password === testUser.password) {
                    // fake token
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: { token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkb21haW4iOiJ0cmFpbHNsYXNoIiwib3JpZ19pYXQiOjE0OTc3NzkyMTcsImRlZnMiOnsiYnJhbmNoIjoiNTMtaGxhdm55LXNrbGFkMTAyMDEyIn0sImV4cCI6MTQ5ODEzOTIxNywib3JnaWQiOjIsImZ1bGxuYW1lIjoiSmFrdWIgTWFuYSIsImVtYWlsIjoiamFrdWJAdHJhaWxzbGFzaC5jb20iLCJ1c2VyX2lkIjoiNjdkZDJiODQtMmY4MS00ZjAxLWI0OTItMjJjYjUyYzFlMzI0IiwidXNlcm5hbWUiOiJqYWt1YkB0cmFpbHNsYXNoLmNvbSIsImlzcyI6IlRQU09GVCwgcy5yLm8uIn0.-AENdZQfpLPSAW2cybsqY2e6ncmNr_-4TSi_DW6p95g' } })
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200 })
                    ));
                }
            }

        }, 500);

    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions]
};

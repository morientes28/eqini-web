﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {environment} from "../../../environments/environment";
import {CurrentUser, CurrentData} from "../../shared/shared";
import {SharedService} from "../../shared/shared.service";
import {Branch} from "../../setting/branch/branch.model";

@Injectable()
export class AuthenticationService extends SharedService{

    private api: string = environment.backendApiUrl;
    public token: string;

    constructor(private http: Http) {
        super();
        // set token if saved in local storage
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

  /**
   * Autorizacia na backend. Ak je uspesna, tak backend vrati objekt, ktory obsahuje token, email a compnay
   * Ak je autorizaci neuspesna, backend vrati Http status 401 bez tokenu
   * @param username (emailova adresa)
   * @param password
   * @returns {Observable<R|T>}
   */
    login(username: string, password: string): Observable<boolean> {

      // '/account/authenticate/'
        return this.http.post(this.api + '/auth/token/get/', JSON.stringify({ email: username, password: password }),
          {headers: this.getHeadersWithoutAuthorization()})
            .map((response: Response) => {

                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;
                    let parseObject:any = AuthenticationService.parseJwt(this.token);

                    let currentUser = new CurrentUser(response.json().token,
                                                      parseObject.email,
                                                      parseObject.domain);

                    let currentData = this.prepareCurrentData(parseObject.email,
                                                              parseObject.domain);

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(currentUser));
                    localStorage.setItem('currentData', JSON.stringify(currentData));

                    console.log(JSON.stringify(currentUser));
                    console.log(JSON.stringify(currentData));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }).catch(this.handleErrorObservable);
    }

  private handleErrorObservable (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  /**
   * Odhlasenie usera automaticky vymaze z cookies usera
   */
  logout(): void {
      // clear token remove user from local storage to log user out
      this.token = null;
      localStorage.removeItem('currentUser');
  }

  /**
   * Dekodovanie JWT token z backendu
   * @param token
   * @returns {any}
   */
  private static parseJwt(token:string):Object {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    console.log("___"+base64);
    return JSON.parse(window.atob(base64));
  }

  private prepareCurrentData(email:string, company: string ):CurrentData {
    let branch = new Branch(1, "Vyskumne pracovisko BB");
    let period = "2017";
    // todo: z domeny zistit nazov firmy
    return new CurrentData(branch, period, company);
  }
}

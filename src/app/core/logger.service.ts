import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {

  private success = [
  'background: blue',
  'color: white',
  'display: block',
  'text-align: left'
  ].join(';');

  private warning = [
    'background: yellow',
    'color: black',
    'display: block',
    'text-align: center'
  ].join(';');

  private failure = [
  'background: red',
  'color: white',
  'display: block',
  'text-align: center'
].join(';');


  log(msg: string, clazz: Object=null) {
    if (clazz==null)
      console.log('%c %s', this.success, msg);
    else
      console.log('%c %s: %s', this.success, clazz.constructor.name, msg);
  }

  warn(msg: string, clazz: Object=null) {
    if (clazz==null)
      console.warn('%c %s', this.warning, msg);
    else
      console.warn('%c %s: %s', this.warning, clazz.constructor.name, msg);
  }

  error(msg: string, clazz: Object=null) {
    if (clazz==null)
      console.error('%c %s', this.failure, msg);
    else
      console.error('%c %s: %s', this.failure, clazz.constructor.name, msg);
  }

  dump(object: Object, clazz: Object=null){
    console.log('%c %O: %s', this.success, object, clazz);
  }

  /**
   * logovanie servisu
   * @param msg - sprava,  ktora sa zaloguje
   * @param object - parametre volania
   */
  info(msg: string, object: Object=null){
    console.log('%c %s %O', this.success, msg, object);
  }
}

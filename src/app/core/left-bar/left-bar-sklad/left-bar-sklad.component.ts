import { Component, OnInit } from '@angular/core';
import { Router} from "@angular/router";
declare var $ :any;

@Component({
  selector: 'app-left-bar-sklad',
  templateUrl: './left-bar-sklad.component.html',
  styleUrls: ['./left-bar-sklad.component.css']
})
export class LeftBarSkladComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onClick(link:string) {
    //fixme: doriesit
    this.router.navigate([ link ]);
    //window.location.href= link;
  }

  onToggle(clazz){
    let selector = clazz;
    if ($(selector).attr('class').indexOf('toggled')>-1){
      $(selector).removeClass('cat__menu-left__item cat__menu-left__submenu-toggled')
        .addClass('cat__menu-left__item cat__menu-left__submenu');
      //$(selector).addClass('cat__menu-left__item cat__menu-left__submenu');
    } else {
      $(selector).removeClass('cat__menu-left__item cat__menu-left__submenu')
        .addClass('cat__menu-left__item cat__menu-left__submenu-toggled');
      //$(selector).addClass('cat__menu-left__item cat__menu-left__submenu-toggled');
    }

  }
}

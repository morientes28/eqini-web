import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-left-bar',
  templateUrl: './left-bar.component.html',
  styleUrls: ['./left-bar.component.css']
})
export class LeftBarComponent implements OnInit{


  // typ menu - sklad / podvojne-uctovnictvo
  @Input() type:string;

  @Input() isFullScreen;

  constructor() {

  }

  ngOnInit() {

  }

}

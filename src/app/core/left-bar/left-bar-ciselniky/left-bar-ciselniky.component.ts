import {Component, OnInit} from '@angular/core';
import {CiselnikSectionList, CiselnikSection} from "../../../shared/shared.enums";
import {Router} from "@angular/router";


@Component({
  selector: 'app-left-bar-ciselniky',
  templateUrl: './left-bar-ciselniky.component.html',
  styleUrls: ['./left-bar-ciselniky.component.css'],
})
export class LeftBarCiselnikyComponent implements OnInit {

  listCiselnikov: Set<CiselnikSection>;

  constructor(private router: Router) { }

  ngOnInit() {
    this.listCiselnikov = CiselnikSectionList.list();
  }

  navigateTo(item) {
    this.router.navigate(['/ciselniky', item]);
  }

}

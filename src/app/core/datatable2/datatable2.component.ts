import {Component, AfterViewChecked, Input} from '@angular/core';
import {Source2} from "../datatable/datatable.model";
declare var $ :any;

@Component({
  selector: 'app-datatable2',
  templateUrl: './datatable2.component.html',
  styleUrls: ['./datatable2.component.css']
})
export class Datatable2Component implements AfterViewChecked {

  @Input() source: Source2;
  @Input() _update:any;
  @Input() _delete:any;
  @Input() _select:any;

  constructor() { }

  ngAfterViewChecked() {

    if (this.source) {
      let selector = "#" + this.source.id;
      $(selector).DataTable();
    }

  }
}

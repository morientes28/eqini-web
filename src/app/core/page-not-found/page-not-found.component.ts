import { Component } from '@angular/core';
import {UnitType, UnitTypeEnum} from "../../shared/shared.enums";

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent {

  unit : UnitType = new UnitType(UnitTypeEnum.DESHBOARD);

  constructor() { }

  onBack(){
    window.history.back();
  }

}

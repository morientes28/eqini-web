import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from "./top-bar/top-bar.component";
import { FooterComponent } from "./footer/footer.component";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { LeftBarComponent } from './left-bar/left-bar.component';
import { TopBarPodUctoComponent } from './top-bar/top-bar-pod-ucto/top-bar-pod-ucto.component';
import { TopBarSkladComponent } from './top-bar/top-bar-sklad/top-bar-sklad.component';
import { TopBarDashboardComponent } from './top-bar/top-bar-dashboard/top-bar-dashboard.component';
import { LeftBarSkladComponent } from './left-bar/left-bar-sklad/left-bar-sklad.component';
import { LeftBarPodUctoComponent } from './left-bar/left-bar-pod-ucto/left-bar-pod-ucto.component';
import { LeftBarDashboardComponent } from './left-bar/left-bar-dashboard/left-bar-dashboard.component';
import { ButtonPipe } from './datatable/button.pipe';
import {LoggerService} from "./logger.service";
import {DatatableComponent} from "./datatable/datatable.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LeftBarCiselnikyComponent } from './left-bar/left-bar-ciselniky/left-bar-ciselniky.component';
import { MultiselectComponent } from './multiselect/multiselect.component';
import {FormsModule} from "@angular/forms";
import { Datatable2Component } from './datatable2/datatable2.component';
import { TableComponent } from './table/table.component';
import {
  MatPaginatorModule, MatTableModule, MatSortModule,
  MatInputModule, MatDialogModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MultiboxesComponent } from './dialog/multiboxes/multiboxes.component';
import { DialogComponent } from './dialog/multiboxes/dialog.component';
import { DialogCreateComponent } from './dialog/dialog-create.component';
import { CreateComponent } from './form/ciselniky/create.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  declarations: [
    TopBarComponent,
    FooterComponent,
    LeftBarComponent,
    TopBarPodUctoComponent,
    TopBarSkladComponent,
    TopBarDashboardComponent,
    LeftBarSkladComponent,
    LeftBarPodUctoComponent,
    LeftBarDashboardComponent,
    ButtonPipe,
    PageNotFoundComponent,
    LeftBarCiselnikyComponent,
    DatatableComponent,
    MultiselectComponent,
    Datatable2Component,
    TableComponent,
    MultiboxesComponent,
    DialogComponent,
    DialogCreateComponent,
    CreateComponent,
  ],
  entryComponents: [
    DialogComponent,
    DialogCreateComponent
  ],
  exports: [
    TopBarComponent,
    FooterComponent,
    LeftBarComponent,
    ButtonPipe,
    DatatableComponent,
    Datatable2Component,
    MultiselectComponent,
    TableComponent,
  ],
  providers: [LoggerService]
})
export class CoreModule { }

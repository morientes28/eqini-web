import {Component, ViewChild, ElementRef, Input, EventEmitter} from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {MatPaginator, MatSort, MatDialog} from '@angular/material';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {Ciselnik} from "../../units/ciselniky/ciselnik.model";
import {CiselnikyService} from "../../units/ciselniky/ciselniky.service";
import {CiselnikSection} from "../../shared/shared.enums";
import {DialogCreateComponent} from "../dialog/dialog-create.component";
import {FlashMessagesService} from "angular2-flash-messages";
declare var $ :any;

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers:[CiselnikyService]
})
export class TableComponent {

  @Input() section:CiselnikSection;

  // todo: ziskat z ciselnika ktore stlpce sa maju zobrazit
  displayedColumns: string[] = [];
  exampleDatabase : ExampleDatabase | null;//new ExampleDatabase();
  dataSource: ExampleDataSource | null;

  // todo: implementovat pole vybranych hodnot, nie len jeden riadok
  selectedRowIndex: string = "";

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;
  @ViewChild('filterColumn') filterColumn: ElementRef;

  constructor(public service:CiselnikyService,
              public dialog: MatDialog,
              private flashMessagesService: FlashMessagesService) {

  }

  ngOnInit() {
    this.displayedColumns = this.section.displayedColumns;
    this.exampleDatabase = new ExampleDatabase(this.service, this.section);

    this.dataSource = new ExampleDataSource(
      this.exampleDatabase,
      this.paginator,
      this.sort);

    Observable.fromEvent(this.filter.nativeElement, 'keyup')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) { return; }
        this.dataSource.filterColumn = this.filterColumn.nativeElement.value;
        this.dataSource.filter = this.filter.nativeElement.value;
      });

  }

  applyFilterColumn(){
    this.dataSource.filterColumn = this.filterColumn.nativeElement.value;
    this.dataSource.filter = this.filter.nativeElement.value;
  }

  highlight(row) {
    if (row.id == this.selectedRowIndex) {
      this.selectedRowIndex = "";
  } else {
      this.selectedRowIndex = row.id;
      let selectorSel = "#ch-b-"+row.shortcut;
      $(selectorSel).attr("checked", true);
      let selector = ".table-checkbox";
      $(selector).each( function(){
        $(this).css({"visibility":"visible"});
        $(this).css({"display":"block"});
      });
    }
  }

  enableCheckbox(row){
    let selectorSel = "#ch-b-"+row.shortcut;
    if ( $(selectorSel).prop("checked")==false &&
         $(selectorSel).css("visibility")=="hidden" &&
      this.isSomeCheckboxChecked() ==false){
      $(selectorSel).css({"visibility":"visible"});
      $(selectorSel).css({"display":"block"});
    }
  }

  disableCheckbox(row) {
    let selectorSel = "#ch-b-" + row.shortcut;
    if ($(selectorSel).prop("checked") == false &&
      $(selectorSel).css("visibility") == "visible" &&
      this.isSomeCheckboxChecked() == false) {
      let selector = ".table-checkbox";
      $(selector).each(function () {
        $(this).css({"visibility": "hidden"});
        $(this).css({"display": "block"});
      });
    }

  }

  isSomeCheckboxChecked():boolean{

    let selector = ".table-checkbox:checked";
    return ($(selector).length > 0);

  }

  idsFromCheckbox():string[]{
    let result:string[] = [];
    let selector = ".table-checkbox";
    $(selector).each(function () {
      if ($(this).prop("checked"))
        result.push($(this).val());
    });
    return result;
  }

  removeItems() {

    if (this.selectedRowIndex != "") {
      // todo: implementovat krajsi confirm dialog
      let confirm = window.confirm("Naozaj vykonat akciu ?");
      if (!confirm) return;
      this.flashMessagesService.show('Akcia bola vykonaná!', {
        cssClass: 'alert-success',
        timeout: 3000
      });
      this.selectedRowIndex = "";


      let ids = this.idsFromCheckbox();
      for (let i = 0; i < ids.length; i++) {
        this.service.delete(ids[i], this.section).subscribe(() => {
        });
      }
      // todo: refresh komponenty bez reloadu stranky
      //this.refresh();
    }
  }

  new(){
    let dialogRef = this.dialog.open(DialogCreateComponent, {
        width: '600px',
        data: { section: this.section}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });

  }

  edit(row=null){
    if (row)
      this.highlight(row);
    this.service.getPrivate(this.selectedRowIndex, this.section).subscribe((ciselnik) => {
      let dialogRef = this.dialog.open(DialogCreateComponent, {
        width: '600px',
        height: '440px',
        data: {
          section: this.section,
          ciselnik: ciselnik,
        }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    });

  }

  exchange(){
    // todo: implementovat
    console.log("exchange");
  }

  print(){
    // todo: implementovat
    console.log("print");
  }

  exportData(){
    // todo: implementovat
    console.log("exportData");
  }

  importData(){
    // todo: implementovat
    console.log("importData");
  }

  refresh(){
    location.reload();
  }

}

export interface UserData {
  id: string;
  name: string;
  shortcut: string;
  currency: any;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleDatabase {
  /** Stream that emits whenever the data has been modified. */
  dataChange: BehaviorSubject<UserData[]> = new BehaviorSubject<UserData[]>([]);
  get data(): UserData[] { return this.dataChange.value; }

  constructor(public service:CiselnikyService,
              public section:CiselnikSection) {

    // z ciselnika ziskat lokalne entity
    service.getAllByCompany(section).subscribe(result =>{
      // v loope pridat do tabulky
      for (let i = 0; i < result.length; i++) {
        const copiedData = this.data.slice();
        copiedData.push(this.transform(result[i]));
        this.dataChange.next(copiedData);
      }
    });

  }

  //todo: vymysliet system ako vybrat hodnoty dynamicky pre rozne druhy ciselnika
  transform(item:Ciselnik){
    return {
      id: (item.id).toString(),
      name: item.title,
      shortcut: item.abrv,
      currency: item.currency,
    };
  }
}

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
export class ExampleDataSource extends DataSource<any> {

  _filterChange = new BehaviorSubject('');
  get filter(): string { return this._filterChange.value; }
  set filter(filter: string) {
    this._filterChange.next(filter);
  }
  filterColumn : string;

  constructor(private _exampleDatabase: ExampleDatabase,
              private _paginator: MatPaginator,
              private _sort: MatSort) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<UserData[]> {
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._paginator.page,
      this._sort.sortChange,
      this._filterChange,
    ];

    return Observable.merge(...displayDataChanges).map(() => {

      // zosortuj data
      const data = this.getSortedData();

      // ak je filter vyplneny tak filtruj data
      if (this.filter.length > 0) {
        return data.filter((item: UserData) => {

          let ss:any = item.currency;
          let searchStr = (item.name + item.shortcut).toLowerCase();
          if (ss)
            searchStr = (item.name + item.shortcut + ss.abrv + ss.market).toLowerCase();

          //todo: filter pre konkretny ciselnik

          if (this.filterColumn=="name")
            searchStr = (item.name).toLowerCase();
          if (this.filterColumn=="shortcut")
            searchStr = (item.shortcut).toLowerCase();
          if (this.filterColumn=="currency")
            if (ss)
              return ss.abrv.toLowerCase().indexOf(this.filter.toLowerCase()) != -1;
          if (this.filterColumn=="market")
            if (ss)
              return ('' + ss.market).indexOf(this.filter) != -1;

          return searchStr.indexOf(this.filter.toLowerCase()) != -1;
        });
      }

      // zobraz len cast dat (paginacia)
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      return data.splice(startIndex, this._paginator.pageSize);
    });
  }

  disconnect() {}

  /** Returns a sorted copy of the database data. */
  getSortedData(): UserData[] {
    const data = this._exampleDatabase.data.slice();
    if (!this._sort.active || this._sort.direction == '') { return data; }

    return data.sort((a, b) => {
      let propertyA: number|string = '';
      let propertyB: number|string = '';

      //todo: filter pre konkretny ciselnik
      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'shortcut': [propertyA, propertyB] = [a.shortcut, b.shortcut]; break;
        case 'currency': [propertyA, propertyB] = [a.currency, b.currency]; break;
      }

      let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      let valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction == 'asc' ? 1 : -1);
    });
  }

}


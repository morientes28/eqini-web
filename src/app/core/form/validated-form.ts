import {NgForm} from "@angular/forms";
import {ViewChild} from "@angular/core";

export class ValidatedForm{

  @ViewChild('modelForm') currentForm: NgForm;

  public modelForm: NgForm;

  validationMessages: any;
  formErrors: any;

  ngAfterViewChecked() {
    this.formChanged();
  }

  formChanged() {
    if (this.currentForm === this.modelForm) { return; }
    this.modelForm = this.currentForm;
    if (this.modelForm) {
      this.modelForm.valueChanges
        .subscribe(data => this.onValueChanged(data));
    }
  }

  onValueChanged(data?: any) {
    if (!this.modelForm) { return; }
    const form = this.modelForm.form;

    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }
}

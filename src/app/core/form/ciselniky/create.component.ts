import {Component, OnInit, Input, ViewChild, SimpleChanges, SimpleChange} from '@angular/core';
import {CiselnikSection, CiselnikSectionList, Section} from "../../../shared/shared.enums";
import {Ciselnik} from "../../../units/ciselniky/ciselnik.model";
import {NgForm} from "@angular/forms";
import {FlashMessagesService} from "angular2-flash-messages";
import {CiselnikyService} from "../../../units/ciselniky/ciselniky.service";
import {ValidatedForm} from "../validated-form";
declare var $ :any;

@Component({
  selector: 'app-ciselniky-form-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [ CiselnikyService ]
})
export class CreateComponent extends ValidatedForm implements OnInit {

  @Input() ciselnik : Ciselnik = new Ciselnik();
  @Input() section:CiselnikSection;
  currencyList: Ciselnik[] = [];
  statesList: Ciselnik[] = [];

  @ViewChild('modelForm') currentForm: NgForm;

  constructor(private service: CiselnikyService,
              private flashMessagesService: FlashMessagesService) {
    super();
  }

  ngOnInit() {

    this.validationMessages = {
      'title': {
        'required':      'Názov je vyžadovaný.',
        'not_unique':    'Položka sa už v databáze nachádza'
      },
      'abrv':{
        'maxlength':     'Max 16 znakov.',
        'required':      'Kódové označenie je vyžadované.',
        'not_unique':    'Kódové označenie sa už v databáze nachádza',
      },
    };
    // resetovanie errorov
    this.formErrors = {
      'title': '',
      'abrv': '',
    };

    if (this.ciselnik == null) {
      this.ciselnik = new Ciselnik();
    }
    let sec:CiselnikSection  = CiselnikSectionList.valueOfCiselnikSection("meny");
    this.service.getAll(sec).subscribe((list)=>{
      this.currencyList = list;
    });
   this.service.getAll(this.section).subscribe((list)=>{
     this.statesList = list;
   })
  }

  onSubmit(){
    this.ciselnik = this.currentForm.value;
    if (this.currentForm.valid){
      this.save(this.ciselnik);
    }

  }

  onChange() {

    if (this.validateUniqueTitle(this.ciselnik)==false){
      this.formErrors['title'] = this.validationMessages['title']['not_unique'];
    }
    if (this.validateUniqueAbrv(this.ciselnik)==false){
      this.formErrors['abrv'] = this.validationMessages['abrv']['not_unique'];
    }
  }

  save(ciselnik: Ciselnik){

    if (ciselnik.id){
      this.service.update(ciselnik, this.section).subscribe(() => {
        this.flashMessagesService.show('Položka bola uložená!', {
          cssClass: 'alert-success',
          timeout: 3000
        });
        this.refresh();
      });
    } else {

      if (this.validateUnique(ciselnik)==false){
        this.formErrors['title'] = this.validationMessages['title']['not_unique'];
        return;
      }
      this.service.save(ciselnik, this.section).subscribe(() => {
        this.flashMessagesService.show('Položka bola uložená!', {
          cssClass: 'alert-success',
          timeout: 3000
        });
        this.refresh();
      });
    }

  }

  validateUnique(ciselnik:Ciselnik){
    for (let i=0;i<=this.statesList.length;i++) {
      if (this.statesList[i] != null && ciselnik !=null) {

        if (this.statesList[i].abrv == ciselnik.abrv ||
          this.statesList[i].title == ciselnik.title) {
          return false;
        }
      }
    }
    return true;
  }

  validateUniqueTitle(ciselnik:Ciselnik){
    for (let i=0;i<=this.statesList.length;i++) {
      if (this.statesList[i] != null && ciselnik !=null) {

        if (this.statesList[i].title == ciselnik.title) {
          return false;
        }
      }
    }
    return true;
  }
  validateUniqueAbrv(ciselnik:Ciselnik){
    for (let i=0;i<=this.statesList.length;i++) {
      if (this.statesList[i] != null && ciselnik !=null) {

        if (this.statesList[i].abrv == ciselnik.abrv) {
          return false;
        }
      }
    }
    return true;
  }

  closeDialog(){
    this.refresh();
  }

  refresh(){
    //location.reload();
  }
}

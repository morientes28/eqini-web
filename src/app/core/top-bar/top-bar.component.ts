import {Component, OnInit, Input, AfterViewInit} from '@angular/core';
import { UnitType } from '../../shared/shared.enums';
import {Branch} from "../../setting/branch/branch.model";
declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit, AfterViewInit {

  // typ menu - sklad / podvojne-uctovnictvo ...
  @Input() type:string;
  statusBar : string;

  // spravangAfterViewChecked
  @Input() messages : string;

  @Input() isFullScreen;
  @Input() branches: Branch[];
  @Input() period: string;
  @Input() company: string;

  constructor() {

  }

  ngOnInit() {
    this.statusBar = this.type;
  }

  ngAfterViewInit() {
    //onMessagesChange();
    //this.messages = "nieco predsa len";
    $("#messages").html(this.messages);
    $("#messages").show();
    $("#messages").delay(3000).fadeOut();
  }

  toggleScreen(){
    this.isFullScreen = (!this.isFullScreen);
  }
}

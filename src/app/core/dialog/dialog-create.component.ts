import {Component, Inject, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {CiselnikSection} from "../../shared/shared.enums";
import {CiselnikyService} from "../../units/ciselniky/ciselniky.service";
import {NgForm} from "@angular/forms";
import {Ciselnik} from "../../units/ciselniky/ciselnik.model";

@Component({
  selector: 'app-dialog-create',
  templateUrl: './dialog-create.component.html',
  styleUrls: ['./dialog-create.component.css'],
  providers:[CiselnikyService]
})
export class DialogCreateComponent {

  section: CiselnikSection;
  ciselnik: Ciselnik;
  title: string = "Vytvorenie položky";


  constructor(public dialogRef: MatDialogRef<DialogCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public service: CiselnikyService) {

    this.section = data.section;
    this.ciselnik = data.ciselnik;
    if (this.ciselnik != null){
      this.title = "Úprava položky";
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }



}

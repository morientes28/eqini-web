import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material";
import {DialogComponent} from "./dialog.component";
import {Multibox, Ciselnik} from "../../../units/ciselniky/ciselnik.model";
import {CiselnikyService} from "../../../units/ciselniky/ciselniky.service";
import {CiselnikSection, Section, CiselnikSectionList} from "../../../shared/shared.enums";
import { ActivatedRoute, Router } from '@angular/router';
declare var $ :any;

@Component({
  selector: 'app-multiboxes',
  templateUrl: './multiboxes.component.html',
  styleUrls: ['./multiboxes.component.css'],
  providers:[CiselnikyService]
})
export class MultiboxesComponent implements OnInit{

  mA: Ciselnik[] = [];
  mC: Ciselnik[] = [];

  ngOnInit(): void {
    //this.initMultiboxData(this.section);
    this.service.getAll(this.section).subscribe(result =>{
      this.multibox = new Multibox("multibox", result);
    });

  }

  @Input() section:CiselnikSection;

  multibox = new Multibox("multibox", []);
  name: string;

  constructor(public dialog: MatDialog, public service: CiselnikyService,
              private router: Router, public route: ActivatedRoute) {}

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '600px',
      data: { multibox: this.multibox,section: this.section}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      //location.reload();
      //this.router.navigate([this.route.snapshot.url.join('/')]);

    });
  }

}

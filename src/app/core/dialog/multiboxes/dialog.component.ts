import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material";
import {Multibox, Ciselnik} from "../../../units/ciselniky/ciselnik.model";
import {CiselnikSection} from "../../../shared/shared.enums";
import {CiselnikyService} from "../../../units/ciselniky/ciselniky.service";
declare var $ :any;

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  providers:[CiselnikyService]
})
export class DialogComponent {

  multibox: Multibox;
  section: CiselnikSection;

  constructor(public dialogRef: MatDialogRef<DialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public service: CiselnikyService) {

    this.multibox = data.multibox;
    this.section = data.section;

  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  submit() {
    let items = [];
    let source:Ciselnik[] = [];
    let selector = ".ms-elem-selection.ms-selected";
    $(selector).each(function () {
      items.push($(this).attr("data-id"));
    });

    for (let i = 0; i < items.length; i++) {
      // save ciselnik
      this.service.get(items[i],this.section).subscribe((ciselnik)=>{
        this.service.save(ciselnik, this.section).subscribe(()=>{
          console.log("save "+ ciselnik);
        });
      });
    }

    this.dialogRef.close(source);

  }
}

import {Component, Input, AfterViewChecked} from '@angular/core';
import {Multibox} from "../../units/ciselniky/ciselnik.model";
import {CiselnikSection} from "../../shared/shared.enums";
declare var $ :any;

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.css']
})
export class MultiselectComponent implements AfterViewChecked {

  @Input() multibox: Multibox;
  @Input() _submit:any;
  @Input() section:CiselnikSection;

  constructor() { }

  ngAfterViewChecked() {
    if (this.multibox) {
      let selector = "#" + this.multibox.id;

      $(selector).multiSelect({
        afterSelect: function(values){

        },
        afterDeselect: function(values){

        }
      });

      $('#select-all').click(function(){
        $(selector).multiSelect('select_all');
        return false;
      });
      $('#deselect-all').click(function(){
        $(selector).multiSelect('deselect_all');
        return false;
      });
    }
  }

}

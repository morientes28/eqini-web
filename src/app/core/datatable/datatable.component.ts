import {Component, AfterViewChecked, Input} from '@angular/core';
import { Source } from "./datatable.model";
declare var $ :any;

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements AfterViewChecked {

  @Input() source: Source;
  @Input() _update:any;
  @Input() _delete:any;
  @Input() _select:any;

  constructor() { }


  ngAfterViewChecked() {

    if (this.source) {
      let selector = "#" + this.source.id;
      $(selector).DataTable();
    }

  }

}

export class Action {
  constructor(public isSelect:boolean=false,
              public isDelete:boolean=false,
              public isUpdate:boolean=false
  )
  {}
}

/**
 * Samotny riadok tabulky
 * @id - id zaznamu
 * @data - list dat (co item to stringova hodnota zaznamu)
 */
export class Row {
  constructor(public id:string,
              public data:string[]
  )
  {}
}

/**
 * Data tabulky
 * @header - list nazvov stlpcov tabulky
 * @rows - list riadkov tabulky
 * @action - url adresy akcii nad riadkom
 */
export class Data {

  constructor(public header:string[],
              public rows:Row[],
              public action:Action
             )
  {}

}

/**
 * Trieda, ktora drzi informacie o datach. Pouziva sa pri komponente datatable
 * @id - id tabulky
 * @data - samotne data obalene v triede Data
 */
export class Source {

  constructor(public id:string,
              public data:Data
             )
  {}
}


/**
 * Trieda, ktora drzi informacie o datach. Pouziva sa pri komponente datatable2
 * @id - id tabulky
 * @data - samotne data obalene v triede Data2
 */
export class Source2 {

  constructor(public id:string,
              public data:Data2
  )
  {}
}


/**
 * Data tabulky
 * @header - list nazvov stlpcov tabulky
 * @rows - list riadkov tabulky
 * @action - url adresy akcii nad riadkom
 */
export class Data2 {

  constructor(public header:string[],
              public rows:Row[],
  )
  {}

}


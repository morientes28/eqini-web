import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'button'
})
export class ButtonPipe implements PipeTransform {

  transform(value: string, id: string): string {
    return value + ' ' + id;
  }

}

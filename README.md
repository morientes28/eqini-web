# EqiniWeb - Frontend projektu Eqini

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.1.0.

## Struktura projektu ##

src/ - zdrojovy adresar
node_modules/ - kniznice tretich stran !!! nekomitovat
e2e/ - end to end testy
Dockerfile - subor, ktory popisuje docker image

### Pustanie aplikacie v develop rezime ###
cez Angular CLI cize prikazom ng serve

### Buildovanie aplikacie do produkcie ###
ng build --prod 
po vybuildovani sa vytvori adresar *dist*, ktory sa nesmie komitovat !!!

### TODO najblizsi release ###

ciselniky

### status branchov ###

jwt - implementovane Auth0 (my ale pouzivame inu implementaciu generovania jwt)
develop - aktualny vyvoj

